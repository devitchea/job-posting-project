@extends('templates.menus.main')

@section('layout-content')

<section id="wrapper" class="login-register login-sidebar"  style="background-image:url(/vendor/wrappixel/monster-admin/4.2.1/assets/images/background/automation.jpg);">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-monster" method="POST" action="{{ route('register') }}">
                @csrf
                <a href="javascript:void(0)" class="text-center db">
                    <img style="width: 70px;" src="/vendor/wrappixel/monster-admin/4.2.1/assets/images/images/216-2169420_job-icon-png-job-position-icon-png.png" alt="Home" />
                </a>
                <br>
               
                <h3 class="box-title m-t-40 m-b-0">{{ __('Register') }}</h3><small>Create your account and enjoy</small>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input id="name" placeholder="Name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="form-group">
                        <select name="user_gender" id="" class="form-control" required>
                            <option value=""> -- Please Select Gender --</option>
                                @foreach ($genders as $item)
                                    <option name="user_gender" value="{{$item->lookup_text}}">{{ $item->lookup_text }}</option>   
                                @endforeach
                        </select>
                    </div>
                <div class="form-group m-t-20">
                        <div class="col-xs-12">
                            <input id="name" placeholder="Phone Number" type="number" class="form-control{{ $errors->has('user_phone') ? ' is-invalid' : '' }}" name="user_phone" value="{{ old('user_phone') }}" required autofocus>
                        </div>
                    </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="form-group">
                        <select name="user_dep" id="" class="form-control" required>
                                <option value=""> -- Please Select Ticket Status --</option>
                                    @foreach ($Departments as $item)
                                        <option name="user_dep" value="{{$item->id}}">{{ $item->dep_des }}</option>   
                                    @endforeach
                            </select>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary p-t-0">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Register') }}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Already have an account? <a href="{{ route('login') }}" class="text-info m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection
