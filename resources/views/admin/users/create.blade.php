@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-danger" href="{{ url('admin/user/')}}"><i class="mdi mdi-plus-circle"></i>Cancel</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="row">
  <div class="col-lg-12">
      <div class="card">
            <form action="{{ url('admin/user/store') }}/{{ isset($object) ? $object->id: 0}}" method="POST" enctype='multipart/form-data'>
                {{csrf_field()}}
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label>Name</label>
                                  <input type="text" name="name" id="name" class="form-control" value="{{ isset($object) ? $object->name: ''}}" placeholder="Please Enter tic_summary" required>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="">Email</label>
                                  <input type="text" name="email" id="email" class="form-control" value="{{ isset($object) ? $object->email: ''}}" placeholder="Please Enter tic_type" required>
                              </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Phone Number</label>
                                <input type="number" name="user_phone" id="user_phone" class="form-control" value="{{ isset($object) ? $object->user_phone: ''}}" placeholder="Please Enter Vehicle year" required>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                           <div class="col-md-4">
                            <div class="form-group">
                                <label>Gender</label>
                                <select name="user_gender" id="" class="form-control" required>
                                    <option value=""> -- Please Select --</option>
                                        @foreach ($genders as $item)
                                            <option name="user_gender" value="{{$item->lookup_text}}" {{ isset($object) ? ($object->user_gender == $item->lookup_text ? 'selected': ''): ''}}>{{ $item->lookup_text }}</option>   
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Department</label>
                                <select name="user_dep" id="" class="form-control" required>
                                        <option value=""> -- Please Select --</option>
                                    @foreach ($department as $item)
                                        <option name="user_dep" value="{{$item->id}}" {{ isset($object) ? ($object->user_dep == $item->id ? 'selected': ''): ''}}>{{ $item->dep_des }}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="form-actions pull-right">
                      <div class="card-body">
                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                          
                      </div>
                  </div>
            </form>
      </div>
  </div>
</div>
<!-- Row -->

@endsection
