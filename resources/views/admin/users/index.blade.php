@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
            <a class="btn float-right hidden-sm-down btn-success" href="{{ url('admin/user/create')}}"><i class="mdi mdi-plus-circle"></i>Create</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p>
        {{session('success')}}
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Lists User</h4>
                <div class="table-responsive m-t-40">
                    <table id="list_vehicles" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                         <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                         @foreach ($users as $user)
                         <tr>
                            <td>{{$i++}}</td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td> 
                          <td>{{ $user->user_phone }}</td>
                          <td>{{ $user->created_at }}</td>
                          <td>{{ $user->updated_at }}</td>
                          <td>
                              <a title="Edit" href="{{ url('admin/user/show/' . $user->id)}}"><i class="mdi mdi-account-edit"></i>Edit</a> | 
                              <a title="Delete" href="{{ url('admin/user/destroy/' .$user->id)}}"><i class="mdi mdi-delete text-danger"></i>Delete</a> 
                            </td>
                         </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
  @push('after-scripts')
    <script>
    $(function() {
        $('#list_vehicles').DataTable();
    });
    </script>
  @endpush  

@endsection
