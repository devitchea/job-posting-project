@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-danger" href="{{ url('admin/role/')}}"><i class="mdi mdi-plus-circle"></i>Cancel</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="row">
  <div class="col-lg-12">
      <div class="card">
            <form action="{{ url('admin/role/store') }}/{{ isset($object) ? $object->id: 0}}" method="POST" enctype='multipart/form-data'>
                {{csrf_field()}}
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label for="">Role</label>
                                  <input type="text" name="name" id="name" class="form-control" value="{{ isset($object) ? $object->name: ''}}" placeholder="Please Enter role" required>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="">Detail</label>
                            <textarea name="remark" id="remark" class="form-control" cols="30" rows="10">{{ isset($object) ? $object->remark: ''}}</textarea>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions pull-right">
                      <div class="card-body">
                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                          
                      </div>
                  </div>
            </form>
      </div>
  </div>
</div>
<!-- Row -->

@endsection
