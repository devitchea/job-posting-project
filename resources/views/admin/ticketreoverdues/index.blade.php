@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Tickets Overdue' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Tickets Overdue' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-success" href="{{ url('admin/ticket/create')}}"><i class="mdi mdi-plus-circle"></i>Create</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p>
        {{session('success')}}
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{ isset($title) ? $title : 'Tickets Overdue' }}</h4>
                <div class="table-responsive m-t-40">
                    <table id="list_vehicles" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                         <th>Number</th>
                         <th>Ticket Summary</th>
                         <th>Date Created</th>
                            <th>Ticket Detail</th>
                            <th>Tiket Type</th>
                            <th>From</th>
                            <th>Department</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                         @foreach ($tickets as $item)
                         <tr>
                            <td>000{{$i++}}</td>
                            <td>{{ $item->tic_summary }}</td>
                            <td>{{ $item->created_at }}</td>
                          <td>{{ $item->tic_detail }}</td>
                          <td>{{ $item->tic_type }}</td>
                          <td>{{ $item->userid['name'] }}</td>
                           @foreach ($departments as $item1)
                              @if($item->userid['user_dep'] == $item1->id) 
                              <td>{{ $item1->dep_des }}</td>
                              @endif
                          @endforeach
                          <td>
                                <a title="Edit" href="{{ url('/admin/ticket/show/'. $item->id)}}"><i class="fas fa-edit"></i></a> | 
                                <a title="Delete" onclick="return confirm('Are you sure you want to delete?')" href="{{ url('/admin/ticket/destroy/'. $item->id)}}"><i class="fas fa-trash-alt text-danger"></i></a> | 
                                <a title="Detail" href="{{ url('/admin/ticket/detail/'. $item->id)}}"><i class="fas fa-tasks text-info"></i></a> 
                            </td>
                         </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
  @push('after-scripts')
    <script>
    $(function() {
        $('#list_vehicles').DataTable();
    });
    </script>
  @endpush  

@endsection
