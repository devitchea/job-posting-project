@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-danger" href="{{ url('admin/ticket/')}}"><i class="mdi mdi-plus-circle"></i>Cancel</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="row">
  <div class="col-lg-12">
      <div class="card">
            <form action="{{ url('admin/ticket/store') }}/{{ isset($object) ? $object->id: 0}}" method="POST" enctype='multipart/form-data'>
                {{csrf_field()}}
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-12">
                                <div class="card-header">
                                    <h3>Ticket #000{{ isset($object) ? $object->id : '' }}</h3>
                                    <h3>Title: <label for="">{{ isset($object) ? $object->tic_summary : '' }}</label></h3>
                                </div>
                          </div>
                      </div>
                    <br>
                      <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>Status </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->tic_status : ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Priority </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->tic_priority: ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Department </label></div>
                                    <div class="col-md-1">:</div>
                                    @foreach ($departments as $item)
                                        <div class="col-md-6">
                                            <label for="">
                                                <strong>{{ isset($object) ? ($object->userid['user_dep'] == $item->id) ? $item->dep_des : '' : ''}}</strong>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Create Date </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->created_at : ''}}</strong></label></div>
                                </div>
                                
                                {{-- <div class="row">
                                    <div class="col-md-5"><label>Vehicle Photo</div>
                                    <div class="col-md-1">: </div>
                                    <div class="col-md-6">
                                         @if($object->image == '')
                                            <img id="output" src="https://www.tutsmake.com/wp-content/uploads/2019/01/no-image-tut.png" alt="vehicle1" class="" width="300" height="auto"/>
                                        @else
                                            <img id="output" name="logo" aria-valuemax="{{ $object->image }}" src="{{ asset($object->image)}}" alt="vehicle2" class="" width="300" height="auto"/>
                                        @endif
                                    </div>  
                                </div> --}}
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>User </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->userid['name']: ''}}</strong></label></div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-5"><label>Email </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->userid['email']: ''}}</strong></label></div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-5"><label>Organization </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->plate_number: ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Source </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->plate_number: ''}}</strong></label></div>
                                </div>
                            </div>
                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>Assigned To </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->assignid['name'] : ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>SLA Plan </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? 'Fix All': ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Due Date </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->tic_due_date: ''}}</strong></label></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>Help Topic </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->userid['name']: ''}}</strong></label></div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-5"><label>Last Message </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->tic_due_date: ''}}</strong></label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"><label>Last Response </label></div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6"><label for=""><strong>{{ isset($object) ? $object->plate_number: ''}}</strong></label></div>
                                </div>
                            </div>
                    </div>
                </div>
            </form>
            
            <form action="{{ url('admin/ticket/storecomment') }}/{{ isset($object) ? $object->id: 0}}" method="POST" enctype='multipart/form-data'>
                {{csrf_field()}}
                <div class="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card">
                                        <div class="card-header bg-info text-white">
                                            Comments ro Message
                                        </div>
                                        <div class="card-body">
                                            @foreach ($comments as $item)
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <img class="images2" src="https://img.icons8.com/bubbles/2x/user.png" alt="Avatar">
                                                            <p><strong>{{ $item->com_name }}</strong></p>
                                                            <p>{{ $item->com_detail }}</p>
                                                            <div>
                                                                <img class="img-thumbnail images" src="{{ url(($item->image)) }}">
                                                            </div>
                                                            
                                                            @foreach ($users as $item1)
                                                                @if($item1->id == $item->user_id)
                                                                <span class="time-left">Posted By: <strong>{{ $item1->name }}</strong> </span>
                                                                @endif 
                                                            @endforeach
                                                            <span class="time-right">Posted Date: {{ $item->com_date }} <span><a data-toggle="modal" data-target="#exampleModal" href="javascript:void(0)" id="edit-user" data-id="{{ $item->id }}" class="">Edit</a></span> <span> <a title="Delete Comment"  onclick="return confirm('Are you sure you want to delete?')"  href="{{ url('admin/ticket/destroyMessage/'.$item->id)}}">  Delete</a></span></span>
                                                        </div>
                                                    </div> 
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row card-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Comment Title</label>
                                    <input type="hidden" name="user_id" value="{{ Auth::user()['id'] }}">
                                    <input type="hidden" name="tic_id" value="{{ isset($object) ? $object->id : ''}}">
                                    <input type="hidden" name="emailfrom" value="{{ Auth::user()['email'] }}">
                                    <input type="hidden" name="emailto" value="{{ isset($object) ? $object->assignid->email : ''}}">
                                    <input type="text" name="com_name" class="form-control" placeholder="Please Enter Comment Title">
                                </div>
                                
                                <div class="form-group">
                                    <label for="">Comments To Ticket Detail</label>
                                    <textarea name="com_detail" class="form-control" id="" cols="30" rows="7"></textarea>
                                </div>
                                    <div class="form-group">
                                        <label for="">Screensort</label>
                                    <div class="avatar-upload col-md-8">
                                        <label class="avatar-edit">
                                            <input  type='file' hidden name="com_type" accept='image/*' onchange='openFile(event)'/>
                                            <label for="imageUpload"></label>
                                            <img id="output" src="https://www.tutsmake.com/wp-content/uploads/2019/01/no-image-tut.png" alt="vehicle1" class="" width="150" height="auto"/>
                                                    
                                        </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions pull-right">
                      <div class="card-body">
                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                          <a href="{{ url('/admin/ticket')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                      </div>
                  </div>
            </form>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form action="#" id="userForm" method="POST" enctype='multipart/form-data'>
                            {{csrf_field()}}
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <div class="form-group">
                                    <label for="">Comment Title</label>
                                    <input type="hidden" id="id" name="id">
                                    <input type="text" name="com_name" id="comment_title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Comment Detail</label>
                                    <textarea class="form-control" name="com_detail" id="comment_detail" cols="30" rows="5"></textarea>
                                </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="savecom" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
      </div>
  </div>
</div>

@push('after-scripts')
<script>
$(function() {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    /* When click edit user */
    $('body').on('click', '#edit-user', function () {
        var user_id = $(this).data('id');
        var url = 'http://ticket.local/admin/ticket/editcoment/';
        $.get(url+user_id, function (data) {
            $('#btn-save').val("edit-user");
            $('#id').val(data.id)
            $('#comment_title').val(data.com_name);
            $('#comment_detail').val(data.com_detail);
        });
    });

    $('body').on('click', '#savecom', function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#userForm').serialize(),
          url: " {{ Route('admin.ticket.storecom')}} ",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#exampleModal').modal('hide');
              location.reload();
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
});


var cityOptions = document.querySelectorAll('input[type="file"]');
              
            // image reader
            var openFile = function(event) {
                var input = event.target;
                var reader = new FileReader();
                reader.onload = function(){
                    var dataURL = reader.result;
                    var output = document.getElementById('output');
                    output.src = dataURL;
                };
                reader.readAsDataURL(input.files[0]);
            };
</script>
@endpush
<!-- Row -->
<style>
    /* Chat containers */
.container {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

/* Darker chat container */
.darker {
  border-color: #ccc;
  background-color: #ddd;
}

/* Clear floats */
.container::after {
  content: "";
  clear: both;
  display: table;
}

/* Style images */
.container .images2 {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

/* Style the right image */
.container img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

/* Style time text */
.time-right {
  float: right;
  color: #aaa;
}

/* Style time text */
.time-left {
  float: left;
  color: #999;
}

</style>
@endsection
