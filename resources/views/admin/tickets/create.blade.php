@extends('templates.menus.main')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-danger" href="{{ url('admin/ticket/')}}"><i class="mdi mdi-plus-circle"></i>Cancel</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="row">
  <div class="col-lg-12">
      <div class="card">
            <form action="{{ url('admin/ticket/store') }}/{{ isset($object) ? $object->id: 0}}" method="POST" enctype='multipart/form-data'>
                {{csrf_field()}}
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label>Subject</label>
                                  <input type="hidden" name="user_id" value="{{ Auth::user()['id'] }}">
                                  <input type="text" name="tic_summary" id="tic_summary" class="form-control" value="{{ isset($object) ? $object->tic_summary: ''}}" placeholder="Please Enter subject" required>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label for="">Ticket Type</label>
                                  <input type="text" name="tic_type" id="tic_type" class="form-control" value="{{ isset($object) ? $object->tic_type: ''}}" placeholder="Please Enter tic_type" required>
                              </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Ticket Due Date</label>
                                <input type="date" name="tic_due_date" id="tic_due_date" class="form-control" value="{{ isset($object) ? $object->tic_due_date: ''}}" placeholder="Please Enter Vehicle year" required>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                           <div class="col-md-4">
                            <div class="form-group">
                                <label>Ticket Status</label>
                                <select name="tic_status" id="" class="form-control" required>
                                    <option value=""> -- Please Select Ticket Status --</option>
                                        @foreach ($ticket_status as $item)
                                            <option name="tic_status" value="{{$item->lookup_text}}" {{ isset($object) ? ($object->tic_status == $item->lookup_text ? 'selected': ''): ''}}>{{ $item->lookup_text }}</option>   
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ticket Priority</label>
                                <select name="tic_priority" id="" class="form-control" required>
                                        <option value=""> -- Please Select Ticket Priority --</option>
                                    @foreach ($priority as $item)
                                        <option name="tic_priority" value="{{$item->lookup_text}}" {{ isset($object) ? ($object->tic_priority == $item->lookup_text ? 'selected': ''): ''}}>{{ $item->lookup_text }}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Assign To</label>
                                <select name="assign_id" id="" class="form-control" required>
                                    <option value=""> -- Please Select Assign To --</option>
                                    @foreach ($users as $item)
                                        <option name="assign_id" value="{{$item->id}}" {{ isset($object) ? ($object->assign_id == $item->id ? 'selected': ''): ''}}>{{ $item->name }}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="">Detail</label>
                              <textarea name="tic_detail" id="tic_detail" class="form-control" cols="30" rows="10">{{ isset($object) ? $object->tic_detail: ''}}</textarea>
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="form-actions pull-right">
                      <div class="card-body">
                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                          
                      </div>
                  </div>
            </form>
      </div>
  </div>
</div>
<!-- Row -->

@endsection
