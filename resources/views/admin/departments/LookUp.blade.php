@extends('templates.menus.main')
@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Look Up List</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Look Up List</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
            <a class="btn float-right hidden-sm-down btn-success" href="#"><i class="mdi mdi-plus-circle"></i>Create</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p>
        {{session('success')}}
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Look Up List</h4>
                <div class="table-responsive m-t-40">
                    <table id="list_vehicles" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                         <th>No</th>
                            <th>Look Up Type</th>
                            <th>Look Up Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                         @foreach ($LookUp as $item)
                         <tr>
                            <td>{{$i++}}</td>
                          <td>{{ $item->lookup_type }}</td>
                          <td>{{ $item->lookup_text }}</td>
                          <td>
                                <a title="Edit" href="#"><i class="fas fa-edit"></i></a> | 
                                <a title="Delete" onclick="return confirm('Are you sure you want to delete?')" href="#"><i class="fas fa-trash-alt text-danger"></i></a>
                         </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
  @push('after-scripts')
    <script>
    $(function() {
        $('#list_vehicles').DataTable();
    });
    </script>
  @endpush  

@endsection
