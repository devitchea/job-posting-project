<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ isset($title) ? $title : 'Dashboard' }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">{{ isset($title) ? $title : 'Dashboard' }}</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a class="btn float-right hidden-sm-down btn-success" href=""><i class="mdi mdi-plus-circle"></i>Create</a>
       
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
