<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">

        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">MAIN NAVIGATION</li>
                <li>
                    <a class="" href="{{ url('/') }}" aria-expanded="false">
                        <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Dashboard </span>
                    </a>
                </li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-apps"></i>
                        <span class="hide-menu">Ticket Manager </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{ url('/admin/ticket') }}">
                                <i class="mdi mdi-ticket"></i>
                                <span>Ticket Open </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/ticket/create') }}">
                                <i class="mdi mdi-plus-box"></i>
                                <span>Create Ticket </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/ticketresolved') }}">
                                <i class="mdi mdi-pound-box"></i>
                                <span>Ticket Resolved </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/ticketclose') }}">
                                <i class="mdi mdi-close-network"></i>
                                <span>Ticket Close </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/ticketreoverdue') }}">
                                <i class="mdi mdi-close-network"></i>
                                <span>Ticket Overdue </span>
                            </a>
                        </li>
                    </ul>
                </li>
                @if(Auth::user()['user_role_id'] == 1)
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-key"></i>
                        <span class="hide-menu">Setting </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{ url('/admin/department') }}">
                                <i class="mdi mdi-ticket"></i>
                                <span>Department </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/lookUp') }}">
                                <i class="mdi mdi-plus-box"></i>
                                <span>Look Up Data </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li> 
                    <a class="" href="{{ url('/admin/user') }}" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Users </span>
                    </a>
                </li>
                <li>
                    <a class="" href="{{ url('/admin/role') }}" aria-expanded="false">
                        <i class="mdi mdi-account-settings-variant"></i>
                        <span class="hide-menu">Roles </span>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
