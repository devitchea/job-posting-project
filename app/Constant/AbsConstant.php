<?php
namespace App\Constant;

class AbsConstant
{
    const VALID_RESPONSE = true;

    const INVALID_RESPONSE = false;

    const CAN_ACCESS = true;

    const CANNOT_ACCESS = false;

    const INVALID_TRUE = true;

    const INVALID_FALSE = false;

    const RESPONSE = 'response';

    const MESSAGE = 'message';

    const DATA = 'data';

    const WORD_AND = ' and ';

    const INVALID = 'invalid';

    // Lowercase E in Email
    const L_EMAIL = 'email';

    // Uppercase E in Email
    const U_EMAIL = 'Email';

    const PASSWORD = 'password';

    const POST = 'post';

    const PUT = 'put';

    const OLD_PASSWORD = 'old_password';

    const NEW_PASSWORD = 'new_password';

    const USER_INFO = 'user_info';

    const USER_NAME = 'Username';

    const GUEST = 'guest';

    const USER_AUTH = 'userAuth';

    const LOGOUT = 'logout';

    const NAME = 'name';

    const ICON = 'icon';

    const PATH = 'path';

    const CHILD = 'child';

    const RESULT_FALSE = false;

    const RESULT_TRUE = true;

    const COMPANY = 'company';

    const CONTACTS = 'contacts';

    const ADDRESSES = 'addresses';

    const LOAN_PAYMENT_SCHEDULES = 'loan_payment_schedules';

    const CUSTOMER = 'customer';

    const DEVICE = 'Device';

    const ROLE_NAME = 'Role Name';

    const MODULES = 'modules';

    const ROLES = 'roles';

    const PERMISSIONS = 'permissions';

    const MENU = 'menu';

    const PERIOD = '.';

    const EQUAL = '=';

    const NOT_EQUAL = '!=';

    const IS = 'IS';

    const IS_NOT = 'IS NOT';

    const STATUS_FALSE = '0';

    const STATUS_TRUE = '1';

    const ALLOWED_PATH = ['dashboard', 'user_profile', 'comp_profile', 'password','passport'];

    const ROLE_CUSTOMER = '3';

    const ROLE_COMPANY = '2';

    const ROLE_SUPER_ADMIN = '1';

    const SHOW = 'show';

    const INDEX = 'index';

    const STORE = 'store';

    const PAYMENT_API = 'paymentApi';

    const EDIT = 'edit';

    const DROPDOWN = 'dropdown';

    const SHOWMENU = 'showMenu';

    const CUSTOMERS = 'customers';

    const FETCHLOGGEDINUSER = 'fetchLoggedInUser';

    const SHOWSIDENAV = 'showSideNav';

    const SHOWMODULE = 'showModule';

    const ACTION_DELETE = 'DELETE';

    const ACTION_PUT = 'PUT';

    const ACTION_POST = 'POST';

    const ACTION_GET = 'GET';
    
    const MSPF_BASE_URI = 'https://api-staging.cloud-gms.com/v0/';

    const MSPF_BASE_URI_V1 = 'https://api-staging.cloud-gms.com/v1/';

    const MSPF_BASE_URI_V2 = 'https://api-staging.cloud-gms.com/v2/';

    const STATUS = 'status';

    const PLATE_NUMBER_VALIDATION = 'Plate Number Already Used';

    const LOCAL_TIMEZONE = 'Asia/Manila';

    const CLIENT_ID = 'ca2a607a726d434fa10d1901ef5e9d0a';// 'c7d273a514524450a20f3bbc95b3e749';//7e3842f5620746a8954e1eafcff4d467

    const CLIENT_SECRET = '03b0bd65dc454d97bc1b0ebe240aac90';//'234b55ab4a4a4c3bbacb3daab60e46db';//1731ba81687643929a02ed9a687ba6e8

    const GMS_COMPANY = 1;

    const ID = 'id';

    const LABEL = 'label';

    const TEXT = 'text';

    const VALUE = 'value';

    const SERVICE_ENTITY_ID = "service_entity_id";

    const HEADER = 'header';

    const CURRENCY = '$';

    const TOKEN_LIMIT_HR = 'PT2H';

    const TOKEN = 'access_token';

    const EXPIRES_IN = 'expires_in';

    const TOKEN_TYPE = 'token_type';

    const ERROR = 'error';

    const SERIAL_NO = 'serial number';

    const TERMINAL_ID = 'terminal id';

    const MOBILE_NO = 'mobile number';

    const P_START_TAG = '<p>';

    const P_END_TAG = '</p>';

    const LI_START_TAG = '<li>';

    const LI_END_TAG = '</li>';

    const UL_START_TAG = '<ul>';

    const UL_END_TAG = '</ul>';

    const DIV_START_TAG = '<div style="display: flex;align-items: center;justify-content: center;text-align:left;">';

    const DIV_END_TAG = '</div>';

    const VEHICLE_MAKER = 'Vehicle Maker';

    const VEHICLE_TYPE = 'Vehicle Type';

    const VEHICLE_MODEL = 'Vehicle Model';

    const VEHICLE = 'Vehicle';

    const USER = 'User';

    const USER_ROLE = 'User Role';

    const CUSTOMER_U = 'Customer';

    const UTC = 'UTC';

    const COMPANY_U = 'Company';

    const DEVICE_COMMUNICATING = 'communicating';

    const LINKED = 'linked';

    const REQUEST_URL = '/maps/api/geocode/json';
    
    const GOOGLE_URL = 'https://maps.googleapis.com';

    const NOTIFICATION_DEFAULT_CHANNEL_NAME = 'GMS-IBS-CHANEL';

    const NOTIFICATION_DEFAULT_EVENT_NAME = 'IBS-EVENT-PUSH-NOTIFICATION';

    const NOTIFICATION_TYPE = 'NotificationType';

    const NOTFICATION_NOTIFY = 'Notify';

    const NOTIFICATION_SMS = 'SMS';

    const NOTIFICATION_MAIL = 'Mail';

    const NOTIFICATION_SLACK = 'Slack';

    const NOTIFICABLE_TYPE = 'Remind Due Date';

    const EVENT_DETAIL_STATUS = 'EventDetailStatus';

    const LOOKUP_FOLLOWUP = 'FollowUp';

    const ALERT_STATUS_TYPE = 'Alert Type';

    const ALERT_STATUS_COLLECTION = 'Collection';

    const ALERT_STATUS_CALL = 'Call';

    const ALERT_STATUS_MCCS_CHECK = 'MCCS Check';

    const ALERT_STATUS_FOLLOW_UP = 'Follow up';

    const X_API_KEY = 'x-api-key';
}
