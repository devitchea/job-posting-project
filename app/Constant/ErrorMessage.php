<?php
namespace App\Constant;

class ErrorMessage
{
  const UNABLE_TO_SAVE_USER = 'Unable To Save User';

  const UNABLE_TO_SAVE_MODULE = 'Unable to Save Module';

  const UNABLE_TO_SAVE_USERROLE = 'Unable to Save User Role';

  const UNABLE_TO_SAVE_DEVICE = 'Unable to Save Device';

  const UNABLE_TO_SAVE_USERPERMISSION = 'Unable to Save User Permissions';

  const UNABLE_TO_SAVE_CUSTOMER = 'Unable to Save Customer';

  const UNABLE_TO_SAVE_COMPANY = 'Unable to Save Company';

  const ALREADY_USED =  ' Already Used';

  const MODULE_NAME_ALREADY_USED = 'Module Name Already Used';

  const USER_NOT_FOUND = 'User Not Found';

  const CUSTOMER_NO_NOT_FOUND = 'Customer No. does not exist';

  const MODULE_NOT_FOUND = 'Module Not Found';

  const UNABLE_TO_DELETE = 'Unable to delete';

  const NO_LOGGED_IN_USER = 'No Logged In User';

  const UNABLE_TO_UPDATE_PASSWORD = 'Unable to Update Password';

  const INCORRECT_OLD_PASSWORD = 'Incorrect Old Password';

  const UPDATE_FAILED = 'Update Failed';

  const INVALID_CREDENTIALS = 'Invalid Credentials';

  const THERE_IS_NO_SUCH_MODULE = 'There is no such Module';

  const THERES_NO_CHANGE_IN_PERMISSIONS = 'There\'s no change in Permissions';

  const NO_USERPERMISSION_FOR_USER_ROLE = 'No User Permission for User Role';

  const ADDRESS_NOT_FOUND = 'Address Not Found';

  const PAYMENT_HISTORY_NOT_FOUND = 'Payment Not Found';

  const COMPANY_NOT_FOUND = 'Company Not Found';

  const CONTACT_NOT_FOUND = 'Contact Not Found';

  const CUSTOMER_NOT_FOUND = 'Customer Not Found';

  const USER_ROLE_NOT_FOUND = 'User Role Not Found';

  const DATA_NOT_FOUND = 'Data not found';

  const UNABLE_TO_SAVE_VEHICLE_TYPE = 'Unable To Save Vehicle Type';

  const UNABLE_TO_SAVE_VEHICLE_MAKER = 'Unable To Save Vehicle Maker';

  const UNABLE_TO_SAVE_VEHICLE_MODEL = 'Unable To Save Vehicle Model';

  const UNABLE_TO_SAVE_VEHICLE = 'Unable To Save Vehicle';

  const UNABLE_TO_SAVE_LOAN_PAYMENT_SCHEDULE = 'Unable To Save Loan Payment Schedule';

  const UNABLE_TO_SAVE_PAYMENT = 'Unable To Save Payment';

  const UNABLE_TO_FETCH_PATH_NAME = 'Unable To Fetch Path Name';

  const INVALID_TOKEN = 'INVALID TOKEN';

  const UNABLE_TO_LINK = 'Unable To Link';

  const UNABLE_TO_UNLINK = 'Unable To Unlink';

  const NAME_ALREADY_USED = 'Name Already Used';

  const DUPLICATE_CUSTOMER_NO = 'Duplicate Customer Number kindly check your excel file';

  const EXTERNAL_ERROR_MSG = 'Cannot connect to MSPF';

  const DEVICE_NOT_FOUND = 'Device not found';

  const COMPANY_ALREADY_IN_IBS_DATABASE = 'Company already in IBS Database';

  const THERE_IS_NO_SUCH_COMPANY_IN_MSPF = 'There is no such company in MSPF';

  const INVALID_REQUEST = 'Invalid Request';

  const DUPLICATE_DATA_PLS_CHCK = 'Duplicate data. Please check: ';

  const COMPANY_NOT_FOUND_IN_MSPF = 'Company not found in MSPF';

  const IS_ARE_STILL_ATTACHED_TO_THIS = 'is/are still attached to this ';

  const UNABLE_TO_UPDATE_MOBILE_NO = 'Unable to Update Mobile Number';

  const DEVICE_ON_IDLE = "Status is Idling <br>Can not ";

  const DEVICE_IS_RUNNING = "Status is Running <br>Can not ";

  const EXTERNAL_ERROR_MSG_DUPLICATE = 'The data is already existing in MSPF. Please contact your system administrator.';

  const DEVICE_NOT_LINKED = 'Device not linked';

  const NEW_DEVICE_ERROR = 'Device is newly installed and can\'t be Activate/Deactivate' ;

  const SAME_PASSWORD = 'New password must be different with your old password.';

  const VEHICLE_NOT_LINKED = 'Vehicle not linked';

  const UNABLE_TO_UPDATE_DEVICE = 'Unable to Update Device';

  const UNABLE_TO_TRANSFER_ITEM_ON_MSPF = 'Unable to Transfer to Other Company';
}
