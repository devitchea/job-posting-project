<?php
namespace App\Constant;

class TableColumn
{
  /**
  *
  * Primary Keys
  *
  */
  const ID = 'id';
  
  const USER_ID = 'user_id';

  const MENU_ID = 'menu_id';

  const MODULE_ID = 'module_id';

  const USER_ROLE_ID = 'user_role_id';

  const USER_PERMISSION_ID = 'user_permission_id';

  const ASSOCIATION_ID = 'association_id';

  const USER_INFORMATION_ID = 'user_information_id';

  const ADDRESS_ID = 'address_id';

  const CONTACT_ID = 'contact_id';

  const COMPANY_ID = 'company_id';

  const COMPANY_CREDENTIAL_ID = 'company_credential_id';

  const COMPANY_NAME = 'company_name'; // not in the actual DB

  const CUSTOMER_ID = 'customer_id';

  const LOOKUP_ID = 'lookup_id';

  const DEVICE_ID = 'device_id';

  const MOBILE_NO = 'mobile_no';

  const DEVICE_TYPE = 'device_type';

  const SERIAL_NO = 'serial_no';

  const TERMINAL_ID = 'terminal_id';

  const VEHICLE_TYPE_ID = 'vehicle_type_id';

  const VEHICLE_MAKER_ID = 'vehicle_maker_id';

  const VEHICLE_MODEL_ID = 'vehicle_model_id';

  const VEHICLE_ID = 'vehicle_id';

  const PAYMENT_ID = 'payment_id';

  const AMOUNT_ID = 'amount_id';

  const PAYMENT_HISTORY_ID = 'payment_history_id';

  const HEADER_ID = 'headers_id';

  /**
  *
  * Other Table Columns
  *
  */

  const USER_NAME = 'username';

  const EMAIL = 'email';

  const PASSWORD = 'password';

  const FIRST_NAME = 'first_name';

  const MIDDLE_NAME = 'middle_name';

  const LAST_NAME = 'last_name';

  const FULL_NAME = 'full_name';

  const GENDER = 'gender';

  const BIRTH_DATE = 'birth_date';

  const IS_BLOCKED = 'is_blocked';

  const CAN_ACCESS = 'can_access';

  const REMEMBER_TOKEN = 'remember_token';

  const MENU_ICON = 'menu_icon';

  const MENU_PATH = 'menu_path';

  const MODULE_NAME = 'module_name';

  const PATH_NAME = 'path_name';

  const MODULE_PARENT_ID = 'module_parent_id';

  const MENU_STATUS = 'menu_status';

  const USER_ROLE_NAME = 'user_role_name';

  const STATUS = 'status';

  const ASSOCIATION_NAME = 'association_name';

  const PHONE_NUMBER = 'phone_number';

  const MOBILE_NUMBER = 'mobile_number';

  const ADDRESS_TYPE = 'address_type';

  const ADDRESS = 'address';

  const CONTACT_TYPE = 'contact_type';

  const CONTACT_NUMBER = 'contact_number';

  const NAME = 'name';

  const SHORT_NAME = 'short_name';

  const NOTE = 'note';

  const SERVICE_ID = 'service_id';

  const CLIENT_ID = 'client_id';

  const CLIENT_SECRET = 'client_secret';

  const DELETED_AT = 'deleted_at';

  const CREATED_AT = 'created_at';

  const UPDATED_AT = 'updated_at';

  const CREATED_BY = 'created_by';

  const UPDATED_BY = 'updated_by';

  const LOOKUP_TEXT = 'lookup_text';

  const LOOKUP_TYPE = 'lookup_type';

  const LOOKUP_STATUS = 'status';

  const SERIAL_NUMBER = 'serial_number';

  const WITH_SIDE_CAR = 'with_side_car';

  const GMS_NUMBER = 'gms_number';

  const SOCIETY_ID = 'society_id';

  const EXTERNAL_ID = 'external_id';

  const COMPANY_EXTERNAL_ID = 'company_external_id';

  const MOTOR_NUMBER = 'motor_number';

  const PLATE_NUMBER = 'plate_number';

  const CHASIS_NUMBER = 'chasis_number';

  const ROW_ID = 'row_id';

  const DUE_DATE = 'due_date';

  const DUE_AMOUNT = 'due_amount';

  const BALANCE = 'balance';

  const PAYMENT_INFO_ONE = 'payment_info_one';

  const PAYMENT_INFO_TWO = 'payment_info_two';

  const SERVICE_ENTITY_ID = 'service_entity_id';

  const PAID_AMOUNT = 'paid_amount';

  const CUSTOMER_NO = 'customer_no';

  const TOTAL_AMOUNT = 'total_amount';

  const DEVICE_STATUS = 'device_status';

  const GRACE_PERIOD = 'grace_period';

  const HEADER_ONE = 'header_one';

  const HEADER_TWO = 'header_two';

  const DEVICE_EXTERNAL_ID = 'device_external_id';

  const PAID = 'paid';

  const IP_ADDRESS= 'ip_address';

  const PAYMENT_DETAIL_ID= 'payment_detail_id';

  const AMOUNT_PAID = 'amount_paid';

  const AMOUNT_EXCESS = 'amount_excess';

  const HAS_CUSTOMER = 'has_customer';

  const SIM_NUMBER= 'sim_number';

  const HAS_DEVICE = 'has_device';

  const MSPF_ID = 'mspf_id';

  const MSPF_SECRET = 'mspf_secret';

  const IBS_ID = 'ibs_id';

  const IBS_SECRET = 'ibs_secret';

  const LAST_LOGIN = 'last_login';

  const SALES_PERSON = 'sales_person';

  const TOTAL_DUE_AMOUNT = 'total_due_amount';

  const NO_CLIENT_DUES_TODAY = 'no_client_dues_today';

  const NO_CLIENT_W_PAST_DUES = 'no_client_w_past_dues';
  
  const NO_CLIENT_PAID_TODAY = 'no_client_paid_today';

  const CANCELLED_BY = 'cancelled_by';

  const DESCRIPTION = 'description';

  const EVENT = 'event';

  const TITLE = 'title';

  const MESSAGE = 'message';

  const TYPE = 'type';

  const NOTIFIABLE = 'notifiable';

  const CHANNEL_ID = 'channel_id';

  const NOTIFICATION_ID = 'notification_id';

  const SUBSCRIBE_ID = 'subscribe_id';

  const COMMENT = 'comment';

  const IS_DELAY = 'is_delay';

  const ALERT_RULE_ID = 'alert_rule_id';

  const ITEM_ID = 'itemId';

  const EVENT_CLASS = 'class';

  const KEY = 'key';

  const VALUE = 'value';

  const PLAYER_ID = 'player_id';

  const ALERTS_STATUS_TYPE_ID = 'alerts_status_type_id';

  const LOOKUP_STATUS_TYPE = 'lookup_status';

  const SECRET = 'secret';

  const REDIRECT = 'redirect';

  const PERSIONAL_ACCESS_CLIENT = 'personal_access_client';

  const PASSWORD_CLIENT = 'password_client';

  const REVOKED =  'revoked';

  const SCOPE = 'scopes';

  const EXPIRES_AT = 'expires_at';

  const REMARK = 'remark';

  const USERS_ID = 'users_id';

  const USER_ROLES_ID = 'user_roles_id';

  const JOIN_DATE = 'join_date';

  const DOB = 'dob';

  const PHONE = 'phone';

  const COMPANIES_ID = 'companies_id';

  const THRESHOLD_COUNT = 'threshold_count';

  const LOCKOUT_DURATION = 'lockout_duration';

  const OTP = 'otp';

  const LOCKOUT_AT = 'lockout_at';

  const THRESHOLD_DURATION = 'threshold_duration';

  const VERIFIED_AT = 'verified_at';

  const AVATAR = 'avatar';

  const RULES_ID = 'rules_id';

  const FORMULAR = 'formular';
  
  const SLUG = 'slug';

  const CONDITION = 'condition';

  const DETAIL = 'detail';

  const CATEGORIES_ID = 'categories_id';

  const PRICE = 'price';

}
