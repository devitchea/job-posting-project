<?php
namespace App\Constant;

class LoggingMessage
{
  const START_MESSAGE = ' PROCESS IS STARTING';

  const AUTO_DEACTIVATION = 'AUTO DEACTIVATION';

  const AUTO_ACTIVATION = ' AUTO ACTIVATION';

  const HYPHEN = ' - ';

  const NOW_CHECKING = 'Now Checking: ';

  const CANT_CONNECT_TO_MSPF = 'Can\'t connect to MSPF';

  const DEACTIVATED = ' has been successfully deactivated';

  const FAILED_TO_DEACTIVATE = 'Failed to deactivate ';

  const TRY_AGAIN = 'Trying again';

  const RESERVED_DEACTIVATE = ' has been reserved to be deactivated';

  const ACTIVATION_CANCELLED = ' has cancelled Activation ';

  const FAILED_TO_CANCEL_ACTIVATION = 'Failed to cancel activation of ';

  const STILL_HAS_BALANCE = ' still has balance that needs to be paid';

  const AUTO_ACTIVATION_CANCELLED = 'ACTIVATION CANCELLED';

  const STILL_ACTIVE = ' is still active';

  const ACTIVATED = ' has been successfully activated';

  const FAILED_TO_ACTIVATE = 'Failed to activate ';

  const RESERVED_ACTIVATE = ' has been reserved to be activated';

  const DEACTIVATION_CANCELLED = ' has cancelled Deactivation ';

  const FAILED_TO_CANCEL_DEACTIVATION = 'Failed to cancel deactivation of ';

  const STILL_INACTIVE = ' is still inactive';

  const AUTO_DEACTIVATION_CANCELLED = 'DEACTIVATION CANCELLED: ';

  const ALREADY_RESERVED_DEACTIVATE = ' is already reserved to deactivate';

  const ALREADY_RESERVED_ACTIVATE = ' is already reserved to activate';

  const RESERVATION_CANCELLED = ' RESERVATION CANCELLED';

  const FAILED_TO_CANCEL_RESERVATION = 'Failed to cancel reservation';

  const AUTO_DEACTIVATION_DONE = 'AUTO DEACTIVATION PROCESS IS DONE';

  const AUTO_ACTIVATION_DONE = ': AUTO ACTIVATION PROCESS IS DONE';

  const NO_DEVICE_LINKED_YET = ': No device linked yet';

  const MISSING_DEVICE_STATUS = ': Missing status';
}
