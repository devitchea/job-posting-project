<?php
namespace App\Constant;

class Status
{
  /*
  *  Activation Status
  */
  const ACTIVE = "Active";

  const INACTIVE = "Inactive";

  const ACTIVATING = "Activating";

  const DEACTIVATING = "Deactivating";

  const WAITING_TO_ACT = "Waiting to Activate";

  const WAITING_TO_DEACT = "Waiting to Deactivate";

  const RESERVING_TO_ACTIVATE = "Reserved to Activate";

  const RESERVING_TO_DEACTIVATE = "Reserved to Deactivate";

  const ACTIVATED = "Activated";

  const DEACTIVATED = "Deactivated";


  /*
  *  Device Status
  */
  const COMM = "Communicating";

  const SLEEP = "Sleep";

  const NOTCOMM = "Not Communicating";


  /*
  *  Running Status
  */
  const IDLING = "Idling";

  const STOP = "Stop";

  const RUN = "Running";


      /*
      *  Common Status
      */
    const UNKNOWN = "Unknown";
}
