<?php
namespace App\Constant;

class TableName
{
  const USERS = 'users';

  const MENUS = 'menus';

  const MODULES = 'modules';

  const ROLES = 'user_roles';

  const PERMISSIONS = 'user_permissions';

  const ASSOCIATIONS = 'associations';

  const ADDRESSES = 'addresses';

  const CONTACTS = 'contacts';

  const COMPANIES = 'companies';

  const CUSTOMERS = 'customers';

  const LOOKUP = 'lookup';

  const DEVICES = 'devices';

  const VEHICLE_TYPES = 'vehicle_types';

  const VEHICLE_MAKERS = 'vehicle_makers';

  const VEHICLE_MODELS = 'vehicle_models';

  const VEHICLES = 'vehicles';

  const LOAN_PAYMENT_SCHEDULES = 'loan_payment_schedules';

  const PAYMENT_HISTORY = 'payment_history';

  const LOAN_PAYMENT_HEADERS = 'loan_payment_headers';

  const VIEWS_CUSTOMER_COMPANY_LOAN = 'views_customer_company_loan';

  const PAYMENT_DETAILS = 'payment_details';

  const COMPANY_CREDENTIALS = 'company_credentials';

  const VIEWS_COMPANY_ID_KEY = 'views_company_id_key';

  const VIEWS_CUSTOMER_INFORMATION = 'views_customer_information';

  const DELAY = 'delays';

  const ALERT = 'alerts';

  const ALERT_RULE = 'alert_rules';

  const ALERT_STATUS = 'alerts_status';

  const ALERT_STATUS_TYPE = 'alerts_status_types';

  const EVENT = 'events';

  const EVENT_DETAIL = 'event_details';

  const CALLING = 'callings';

  const ALERT_ACTION = 'alerts_have_actions';

  const MCCS_CHECK = 'mccs_checkings';

  const FOLLOWUP = 'follow_ups';

  const ALERT_RULE_DETAIL = 'alert_rule_details';

  const WING_RETAILER_TRANSACTION =  'wings_retailer_transactions';

  const OAUTH_CLIENT = 'oauth_clients';

  const OAUTH_AUTH_CODES = 'oauth_auth_codes';

  const USERS_HAS_USER_ROLES = 'users_has_user_roles';

  const VERIFY_USER = 'verify_users';

  const RULE_DETAILS = 'rule_details';
}
