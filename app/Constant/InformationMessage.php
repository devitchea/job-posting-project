<?php
namespace App\Constant;

class InformationMessage
{
  const SAVED_SUCCESSFULLY = 'Saved Successfully';

  const UPDATED_SUCCESSFULLY = 'Updated Successfully';

  const DELETED_SUCCESSFULLY = 'Deleted Successfully';

  const PASSWORD_UPDATED_SUCCESSFULLY = 'Password Updated Successfully';

  const VALID_CREDENTIALS = 'Valid Credentials';

  const RESET_PASSWORD_SUBJECT = 'Reset Password';

  const RESET_PASSWORD_GREETINGS = 'Hello!';

  const RESET_PASSWORD_FIRST_LINE = 'You are receiving this email because we received a password reset request for your account.';

  const RESET_PASSWORD_BUTTON = 'Reset Password';

  const RESET_PASSWORD_SECOND_LINE = 'If you did not request a password reset, no further action is required.';

  const LINK_SUCCESSFULLY = 'Linked Successfully';

  const UNLINK_SUCCESSFULLY = 'Unlinked Successfully';

  const TRANSFER_UNSUCCESSFUL = 'Unlinked Successfully but Transfer was Unsuccessful';

  const PAYMENT_SUCCESS = 'Payment successfully processed.';

  const TOKEN_MESSAGE = 'Here is your new personal access token. This is the only time it will be shown so don\'t lose it! You may now use this token to make API requests.';

  const  MSG_SMS = 'Send SMS successfully!';

  const NOTIFCATION_CANNOT_SEND = 'Notification cannot send!';

  const CALL_SUCCESS = 'Call to customer success!';

  const CHECK_MCCS_SUCCESS = 'Check Mccs Device success';

  const FOLLOW_UP_SUCCESS = 'Follow up successfuly';
}
