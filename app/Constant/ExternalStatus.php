<?php
namespace App\Constant;

class ExternalStatus
{
  /*
  *  Activation Status
  */
  const ACTIVE = "ACTIVE";

  const INACTIVE = "INACTIVE";

  const ACTIVATING = "ACTIVATING";

  const DEACTIVATING = "DEACTIVATING";

  const WAITING_TO_ACT = "WAITING_TO_ACT";

  const WAITING_TO_DEACT = "WAITING_TO_DEACT";

  const RESERVING_TO_ACTIVATE = "Activate";

  const RESERVING_TO_DEACTIVATE = "Deactivate";


  /*
  *  Device Status
  */
  const COMM = "COMM";

  const SLEEP = "SLEEP";

  const NOTCOMM = "NOTCOMM";


  /*
  *  Running Status
  */
  const IDLING = "IDLING";

  const STOP = "STOP";

  const RUN = "RUN";


    /*
    *  Common Status
    */
  const UNKNOWN = "UNKNOWN";
}
