<?php

namespace App\Providers;

use App\Services\SidebarMenuService;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('sidebarmenuservice', SidebarMenuService::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
