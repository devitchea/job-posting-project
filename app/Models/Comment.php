<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'tic_id',
        'com_name',
        'com_detail',
        'com_date',
        'com_type',
        'image'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    function getImageAttribute($value){
        return $value == null ? env('APP_URL').'images/default.png' : env('APP_URL').$value;
    }
}
