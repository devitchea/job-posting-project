<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tic_summary', 'tic_detail', 'tic_image', 'tic_status', 'tic_type', 'tic_priority', 'tic_due_date', 'avatar', 'assign_id', 'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userid()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignid()
    {
        return $this->belongsTo('App\Models\User', 'assign_id');
    }
}
