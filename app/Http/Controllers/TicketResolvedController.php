<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\User;

class TicketResolvedController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['departments'] = Department::all();
        $data['tickets'] = Ticket::where('tic_status', 'Resolved')->with('userid', 'assignid')->get();
        
        return view('admin.tickets_close.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = User::all();
        $data['tickets'] = Ticket::all();

        return view('admin.tickets_close.create',
            [
                "title" => "Create Ticket",
                "link_create" => 'admin/ticketclose',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0)
    {
        $request->validate([
            'tic_summary'=>'required|min:2|max:255',
        ]);
        $data = array(
            'tic_summary' => $request->tic_summary,
            'tic_detail' => $request->tic_detail,
            'tic_image' => $request->tic_image,
            'tic_status' => $request->tic_status,
            'tic_type' => $request->tic_type,
            'tic_due_date' => $request->tic_due_date,
            'assign_id' => $request->assign_id,
            'user_id' => $request->user_id,
        );
        //dd($data);

        $insert = Ticket::updateOrCreate([
            'id' => $id
        ], $data);
        
        if($insert)
        {
            $request->session()->flash('success', 'Data has been ' .( $id != 0 ? 'Updated !' : 'Saved!'));
            return redirect('admin/ticketclose');
        }
        else
        {
            $request->session()->flash('error','Fail to save data, please check again!');
            return redirect('admin/ticketclose')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['users'] = User::all();
        $data['object'] = Ticket::find($id);
        return view('admin.tickets.create' , $data)->with(
            [
                "title" => "Edit Ticket",
                "link_create" => 'admin/ticketclose',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = Ticket::where('id', $request->id)->delete();
        
        if ($delete) {
            $request->session()->flash('success', 'Data has been deleted!');
            return redirect('admin/ticketclose');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/ticketclose');
        }
    }
}
