<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Company;
use App\Models\Department;
use App\Models\LookUp;

class UserController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::all();
        
        return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['genders'] = LookUp::where("lookup_type", 'genders')->get();
        $data['department'] = Department::all();
        return view('admin.users.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0)
    {
        $request->validate([
            'name' => 'required|min:2|max:255',
        ]);
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'user_gender' => $request->user_gender,
            'user_phone' => $request->user_phone,
            'user_dep' => $request->user_dep,
        );

        $insert = User::updateOrCreate([
            'id' => $id
        ], $data);

        if ($insert) {
            $request->session()->flash('success', 'Data has been ' . ($id != 0 ? 'Updated !' : 'Saved!'));
            return redirect('admin/user');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/user')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['genders'] = LookUp::where("lookup_type", 'genders')->get();
        $data['department'] = Department::all();
        $data['object'] = User::find($id);
        
        return view('admin.users.create')->with($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'user_role_id' => $request->user_role_id,
            'companies_id' => $request->companies_id,
        );

        if ($request->password != "") {
            $data['password'] = bcrypt($request->password);
        }
        
        $update = User::where('id', $request->id)->update($data);
        if($update)
        {
            $request->session()->flash('success', 'Data has been updated!');
            return redirect('admin/user');
        }
        else{
            $request->session()->flash('error','Fail to save data, please check again!');
            return redirect('admin/user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = User::where('id', $request->id)->delete();
        if ($delete) {
            $request->session()->flash('success', 'Data has been deleted!');
            return redirect('admin/user');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/user');
        }
    }
}
