<?php

namespace App\Http\Controllers;
use App\Services\DashboardService;
use DB;
use App\Models\Module;
use App\Models\Ticket;

class MainBoardReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Simulating a connection to a database/webservice, to
     * fetch data, putting it in Javascript/Client-Side
     * So then it will be rendered in the browser
     *
     */
    public function index(DashboardService $dashboardService)
    {
       
        $data['ticket1'] = Ticket::count();
        $data['ticket2'] = Ticket::where('tic_status', 'Closed')->count();
        $data['ticket3'] = Ticket::where('tic_status', 'Resolved')->count();
        $data['ticket4'] = Ticket::where('tic_due_date', '2019-10-18')->count();
        // $garages = DB::table('garages')->count();
        //Fetching data
        $chart2Data = $dashboardService->getChart2Data();
        $chartSales = $dashboardService->getChartSales();
        $salesDifference = $dashboardService->getSalesDifference();
        $salesPrediction = $dashboardService->getSalesPrediction();

        //Passing data to the view
        return view('admin.main-content')
            ->with('chart2Data', $chart2Data)
            ->with('salesPrediction', $salesPrediction)
            ->with('salesDifference', $salesDifference)
            ->with('chartSales', $chartSales)
            ->with($data);
    }

}
