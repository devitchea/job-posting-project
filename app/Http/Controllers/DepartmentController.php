<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\LookUp;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['Departments'] = Department::all();

        return view('admin.departments.index', $data);
    }
    // get look up data
    public function lookUp()
    {
        $data['LookUp'] = LookUp::all();

        return view('admin.departments.LookUp', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'admin.departments.create',
            [
                "title" => "Create Department",
                "link_create" => 'admin/department',
                "link_label" => "Cancel"
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0)
    {
        $data = array(
            'dep_des' => $request->dep_des,
            'remark' => $request->remark,
        );

        $insert = Department::updateOrCreate([
            'id' => $id
        ], $data);

        if ($insert) {
            $request->session()->flash('success', 'Data has been ' . ($id != 0 ? 'Updated !' : 'Saved!'));
            return redirect('admin/department');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/department');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['object'] = Department::find($id);
        return view(
            'admin.departments.create',
            [
                "title" => "Department Name",
                "link_create" => 'admin/department',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = Department::where('id', $request->id)->delete();

        if ($delete) {
            $request->session()->flash('success', 'Data has been deleted!');
            return redirect('admin/department');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/department');
        }
    }
}
