<?php
   
namespace App\Http\Controllers\Auth;
   
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
   
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
   
    use AuthenticatesUsers;
   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   

    // socailite login
    
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
   
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return redirect('auth/google');
        }

        // only allow people with @company.com to login
        if(explode("@", $user->email)[1] !== 'global-mobility-service.com'){
            return redirect()->to('/');
        }

        $authUser = $this->createUser($user);
        Auth::login($authUser, true);
        return redirect()->route('home');
    }

    public function createUser($user)
    {
        $authUser = User::where('google_id', $user->id)->first();
        if($authUser) {
            return $authUser;
        }else {
            return User::create([
                'name' => $user->name,
                'email' => $user->email,
                'google_id' => $user->id,
                'avatar' => $user->avatar,
                'avatar_original' => $user->avatar_original,
            ]);
        }
    }
}