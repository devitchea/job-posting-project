<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\Models\Department;
use App\Models\LookUp;
use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Comment;
use DateTime;
use App\Constant\AbsConstant;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['departments'] = Department::all();
        $data['tickets'] = Ticket::where('tic_status', 'Open')->with('userid', 'assignid')->orderBy('created_at' , 'desc')->get();

        return view('admin.tickets.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['priority'] = LookUp::where('lookup_type', 'priority')->get();
        $data['ticket_status'] = LookUp::where('lookup_type', 'ticket_status')->get();
        $data['users'] = User::all();
        $data['tickets'] = Ticket::all();

        return view(
            'admin.tickets.create',
            [
                "title" => "Create Ticket",
                "link_create" => 'admin/ticket',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0)
    {
        $request->validate([
            'tic_summary' => 'required|min:2|max:255',
        ]);
        $data = array(
            'tic_summary' => $request->tic_summary,
            'tic_detail' => $request->tic_detail,
            'tic_image' => $request->tic_image,
            'tic_status' => $request->tic_status,
            'tic_type' => $request->tic_type,
            'tic_due_date' => $request->tic_due_date,
            'assign_id' => $request->assign_id,
            'user_id' => $request->user_id,
            'tic_priority' => $request->tic_priority,
        );

        $insert = Ticket::updateOrCreate([
            'id' => $id
        ], $data);

        if ($insert) {
            $request->session()->flash('success', 'Data has been ' . ($id != 0 ? 'Updated !' : 'Saved!'));
            return redirect('admin/ticket');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/ticket')->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storecomment(Request $request, $id = 0)
    {
        $emailfrom = $request->emailfrom;
        $emailto = $request->emailto;
        $dt = new DateTime();
        $data = array(
            'user_id' => $request->user_id,
            'tic_id' => $request->tic_id,
            'com_name' => $request->com_name,
            'com_detail' => $request->com_detail,
            'com_date' => $dt,
        );
        // send Email start
        Mail::to($emailto)->send(new SendMail($data));
        // send email end
        $insert = Comment::updateOrCreate([
            'id' => $id
        ], $data);


        if ($request->hasFile('com_type')) {
            $validator_image = $this->validator_image($request->all());
            if ($validator_image->fails()) :
                $response = [
                    AbsConstant::RESPONSE => AbsConstant::INVALID_RESPONSE,
                    AbsConstant::MESSAGE => $validator_image->errors()->first()
                ];
                return $response;
            endif;

            $file = $request->file('com_type');
            $name = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('/images/comment/');
            $file->move($destinationPath, $name);

            $insert->image = 'images/comment/' . $name;
            $insert->save();
        }

        if ($insert) {
            $request->session()->flash('success', 'Data has been ' . ($id != 0 ? 'Updated !' : 'Saved!'));
            return redirect('admin/ticket/detail/' . $id);
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/ticket/detail/' . $id);
        }
    }

    public function editcoment($id)
    {   
        $user = Comment::find($id);
        return response()->json($user);
    }

    public function storecom(Request $request)
    {
        Comment::updateOrCreate(['id' => $request->id],
                ['com_name' => $request->com_name,
                'com_detail' => $request->com_detail,]);        
   
        return response()->json(['success'=>'Saved successfully.']);
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['priority'] = LookUp::where('lookup_type', 'priority')->get();
        $data['ticket_status'] = LookUp::where('lookup_type', 'ticket_status')->get();
        $data['users'] = User::all();
        $data['object'] = Ticket::find($id);
        return view('admin.tickets.create', $data)->with(
            [
                "title" => "Edit Ticket",
                "link_create" => 'admin/ticket',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data['departments'] = Department::all();
        $data['users'] = User::all();
        $data['comments'] = Comment::where('tic_id', $id)->get();
        $data['object'] = Ticket::find($id);
        
        return view('admin.tickets.detail', $data)->with(
            [
                "title" => "Ticket Detail",
                "link_create" => 'admin/ticket',
                "link_label" => "Cancel"
            ]
        )->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = Ticket::where('id', $request->id)->delete();

        if ($delete) {
            $request->session()->flash('success', 'Data has been deleted!');
            return redirect('admin/ticket');
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/ticket');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMessage(Request $request)
    {
        $delete = Comment::where('id', $request->id)->delete();

        if ($delete) {
            $request->session()->flash('success', 'Data has been deleted!');
            return back()->withInput();
        } else {
            $request->session()->flash('error', 'Fail to save data, please check again!');
            return redirect('admin/ticket');
        }
    }

     // image validation // 
     protected function validator_image(array $data)
     {
         return Validator::make($data, [
             'com_type' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
         ]);
     }
}
