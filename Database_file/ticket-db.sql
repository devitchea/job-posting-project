-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Oct 21, 2019 at 01:20 AM
-- Server version: 8.0.16
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `com_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_detail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_date` datetime DEFAULT NULL,
  `com_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `tic_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `com_name`, `com_detail`, `com_date`, `com_type`, `user_id`, `tic_id`, `created_at`, `updated_at`, `image`) VALUES
(26, 'asfasfafsdafdf', 'afasdfdsfadsfa', '2019-10-17 07:33:05', NULL, 1, 4, '2019-10-17 07:33:05', '2019-10-17 07:33:05', NULL),
(27, 'afdafaf', 'afasfasd', '2019-10-17 07:33:10', NULL, 1, 4, '2019-10-17 07:33:10', '2019-10-17 07:33:10', NULL),
(28, 'Image', 'Cards include a few options for working with images. Choose from appending “image caps” at either end of a card, overlaying images with card content, or simply embedding the image in a card.', '2019-10-17 07:33:54', NULL, 2, 4, '2019-10-17 07:33:54', '2019-10-17 07:33:54', NULL),
(29, 'Image 0002``', 'user_id Cards include a few options for working with images. Choose from appending “image caps” at either end of a card, overlaying images with card content, or simply', '2019-10-17 07:35:31', NULL, 2, 4, '2019-10-17 07:35:31', '2019-10-17 07:35:31', NULL),
(41, 'Testing 2', 'for testing 2', '2019-10-17 15:28:19', NULL, 7, 12, '2019-10-17 15:28:23', '2019-10-17 15:28:23', NULL),
(66, 'df', 'adsfasdf', '2019-10-18 10:08:38', NULL, 7, 12, '2019-10-18 10:08:43', '2019-10-18 10:08:43', 'images/comment/1571393323.PNG'),
(67, 'sadfasd', 'asdfas', '2019-10-18 10:20:19', NULL, 7, 6, '2019-10-18 10:20:25', '2019-10-18 10:20:25', 'images/comment/1571394025.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `dep_des` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_des`, `remark`, `created_at`, `updated_at`) VALUES
(1, 'Accounting', 'for Accounting post tikets', NULL, NULL),
(2, 'IT', 'for testing', NULL, NULL),
(3, 'Sale', 'for testiing', NULL, NULL),
(4, 'lll', 'll', '2019-10-17 16:03:20', '2019-10-17 16:03:20'),
(5, 'ggg', 'ggg', '2019-10-18 12:34:04', '2019-10-18 12:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `look_ups`
--

CREATE TABLE `look_ups` (
  `id` int(10) UNSIGNED NOT NULL,
  `lookup_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lookup_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `look_ups`
--

INSERT INTO `look_ups` (`id`, `lookup_type`, `lookup_text`, `status`, `created_at`, `updated_at`) VALUES
(1, 'priority', 'High', '1', NULL, NULL),
(2, 'priority', 'Normal', '1', NULL, NULL),
(3, 'priority', 'Medium', '1', NULL, NULL),
(4, 'ticket_status', 'Open', '1', NULL, NULL),
(5, 'ticket_status', 'Closed', '1', NULL, NULL),
(6, 'ticket_status', 'Resolved', '1', NULL, NULL),
(7, 'genders', 'Male', '1', NULL, NULL),
(8, 'genders', 'Female', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_03_171837_create_users_table', 1),
(2, '2019_09_03_171937_create_password_resets_table', 1),
(3, '2019_09_23_160037_create_roles_table', 1),
(4, '2019_09_23_160243_create_tbl_departments_table', 1),
(5, '2019_09_23_160628_add_field_user_dep_to_users_table', 2),
(6, '2019_09_23_162019_create_tbl_tickets_table', 3),
(7, '2019_09_23_162543_create_tbl_comments_table', 4),
(8, '2019_10_17_022329_create_look_ups_table', 5),
(9, '2019_10_17_044230_add_votes_to_comments_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `tic_summary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tic_detail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tic_image` text COLLATE utf8mb4_unicode_ci,
  `tic_status` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tic_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tic_due_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tic_priority` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `assign_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `tic_summary`, `tic_detail`, `tic_image`, `tic_status`, `tic_type`, `tic_due_date`, `tic_priority`, `user_id`, `assign_id`, `created_at`, `updated_at`) VALUES
(4, 'fdasfasdf', 'dsfsafsadfasd', NULL, 'Closed', 'adsfasdfads', '2019-10-17', 'High', 1, 2, '2019-10-16 17:09:08', '2019-10-17 04:59:29'),
(6, 'fdsfgsfdg', 'sfdgsfdgsdfds', NULL, 'Resolved', 'dsfgsdf', '2019-10-17', 'Normal', 6, 1, '2019-10-17 02:49:39', '2019-10-17 09:26:28'),
(7, 'adfasdf', 'dfasdfsa', NULL, 'Open', 'dfasdf', '2019-10-18', 'Normal', 1, 1, '2019-10-17 03:06:38', '2019-10-17 03:08:56'),
(8, 'asdfasdfafasd', 'fdasfafasdfad', NULL, 'Open', 'asdfasd', '2019-10-19', 'Medium', 1, 1, '2019-10-17 07:04:10', '2019-10-17 07:04:10'),
(9, 'for testing', 'afasdfdasfsad', NULL, 'Open', 'testing', '2019-10-24', 'Normal', 1, 1, '2019-10-17 07:10:23', '2019-10-17 07:10:23'),
(11, 'dfasdfasd', 'fasdfasfasfdsaf', NULL, 'Open', 'afdafads', '2019-10-22', 'Normal', 6, 5, '2019-10-17 08:28:30', '2019-10-17 08:55:54'),
(12, 'User Error', 'For testing Ticket application', NULL, 'Open', 'Testing', '2019-10-23', 'High', 7, 2, '2019-10-17 08:58:15', '2019-10-17 15:17:29'),
(14, 'Testing  didd', 'Testing edit', NULL, 'Open', 'Testing', '2019-10-23', 'High', 7, 7, '2019-10-18 12:29:40', '2019-10-18 12:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_firstname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_dep` int(10) UNSIGNED DEFAULT NULL,
  `user_role_id` int(10) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `user_firstname`, `user_lastname`, `user_gender`, `user_phone`, `user_status`, `user_dep`, `user_role_id`) VALUES
(1, 'User 1', 'devit.chea21998@gmail.com', NULL, '$2y$10$sOAQiJbrKhYoMsQXSOcMiO6JsAaLVYOe9whwO8n1EcvB4kzCPqW5C', 'KMhzHnaCAdPVPv9WDu8B4EERWz5BwtzCTTqp8yx7qjPr1qDHypCvNzbS4PGh', '2019-09-23 16:30:29', '2019-09-23 16:30:29', NULL, NULL, NULL, NULL, NULL, 1, 1),
(2, 'User 2', 'devit.chea21998@gmail.com', NULL, '$2y$10$OOr0t9ESZnhR7AniLN0y1uyNTq6FJBHRToQgmT153gVOPfWARjYFW', NULL, '2019-10-16 15:27:02', '2019-10-16 15:27:02', NULL, NULL, NULL, NULL, NULL, 1, 2),
(5, 'IT Testing', 'it@gmail.com', NULL, '$2y$10$zr4oGqxovfycE3..cnWlRe.qJzcftb224PYrUgd1ITfY.Y/qZ8vwS', 'g2xgzwWnzY0gLVkvvIMisamjBuiNIr8sBb37dvIX33idSUrQUqa6kvw6mreX', '2019-10-17 08:12:20', '2019-10-17 08:12:20', NULL, NULL, NULL, '098765678', NULL, 3, 2),
(6, 'User 3', 'devit.chea21998@gmail.com', NULL, '$2y$10$Y6gSz..27AcO4nL64XLL5u3bQZgFE3UhWDW/402MaNO78JVxpjary', 'kM94vdfZ4BnXdhOi1N5tpJzsU56uGu8OvPoBzg1sUvPXgrP3EWjLIdeTbAVQ', '2019-10-17 08:19:21', '2019-10-17 08:19:21', NULL, NULL, NULL, '098765433', NULL, 3, 1),
(7, 'Super Admin', 'super.admin@gmail.com', NULL, '$2y$10$Hvhy9TqLou1fPAdIv4wAbuUwazp9/dl4Sw9aXL0WN9W1YvSuK/ENq', 'mLywkFN9DoyolE5OoBeX48hD5ZU1DgrJK2v1Nmj39SzLhaaBJes53TNfmKgl', '2019-10-17 09:47:31', '2019-10-17 09:47:31', NULL, NULL, NULL, '0987654323', NULL, 1, 1),
(9, 'Normal', 'normal@gmail.com', NULL, '$2y$10$Sm0GXauhH2gfYu4WDLUA2unw3lgTk4iPFu/RaV5CzL/gWAyO3kwSC', 'TyS0JF4NhyKxPbuYzKlelofAB9z5lJl9eJkZ2OcQaapKfj5i6kHdMpSroy5c', '2019-10-18 11:41:22', '2019-10-18 11:41:22', NULL, NULL, NULL, '098765456', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `remark`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Can access All', NULL, NULL),
(2, 'Normal', 'can access some', NULL, NULL),
(4, 'fasfas', 'afasfasd', '2019-10-17 09:13:40', '2019-10-17 09:13:40'),
(5, 'ICT', 'for testing in the department.', '2019-10-17 15:56:15', '2019-10-17 15:56:15'),
(6, 'Testing', 'for testing', '2019-10-17 15:58:14', '2019-10-17 15:58:14'),
(7, 'froting', 'car testing', '2019-10-17 16:00:17', '2019-10-17 16:00:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_comments_user_id_foreign` (`user_id`),
  ADD KEY `tbl_comments_tic_id_foreign` (`tic_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `look_ups`
--
ALTER TABLE `look_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_tickets_user_id_foreign` (`user_id`),
  ADD KEY `tbl_tickets_assign_id_foreign` (`assign_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_user_dep_foreign` (`user_dep`),
  ADD KEY `users_user_role_id_foreign` (`user_role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `look_ups`
--
ALTER TABLE `look_ups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `tbl_comments_tic_id_foreign` FOREIGN KEY (`tic_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tbl_tickets_assign_id_foreign` FOREIGN KEY (`assign_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_tickets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_user_dep_foreign` FOREIGN KEY (`user_dep`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
