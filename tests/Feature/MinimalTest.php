<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;

class MinimalTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testService()
    {
        $sidebar = App::make('App\Services\SidebarMenuService');
        $pages = $sidebar->getMenuItems();

        foreach ( $pages as $group ) {
            if(is_object($group)) {
                if(property_exists($group, 'children')) {
                   $this->accessPage($group->children);
                }
            }
        }
    }

    public function accessPage($groups){
        foreach ( $groups as $group ) {
            if(property_exists($group, 'url')){

                $response = $this->get($group->url);
                $response->assertStatus(200);

                if(property_exists($group, 'children')) {
                    $this->accessPage($group->children);
                }
            }
        }
    }

    public function testRedirect()
    {
        $response = $this->get('/minimal');
        $response->assertRedirect('/minimal/php-to-js-vars');
    }

    public function testHomeOne()
    {
        $response = $this->get('/minimal/home-one');
        $response->assertStatus(200);
        $response->assertSeeText('Home One');
    }

    public function testHomeTwo()
    {
        $response = $this->get('/minimal/home-two');
        $response->assertStatus(200);
        $response->assertSeeText('Home Two');
    }

    public function testPHPToJSVars()
    {
        $response = $this->get('/minimal/php-to-js-vars');
        $response->assertStatus(200);
    }

    public function testBladeComponents()
    {
        $response = $this->get('/minimal/blade-components');
        $response->assertStatus(200);
    }

    public function testBladeLoops()
    {
        $response = $this->get('/minimal/blade-loops');
        $response->assertStatus(200);
    }

    public function testBoxed()
    {
        $response = $this->get('/minimal/demos/boxed');
        $response->assertStatus(200);
    }

    public function testLogoCenter()
    {
        $response = $this->get('/minimal/demos/logo-center');
        $response->assertStatus(200);
    }

    public function testSingleColumn()
    {
        $response = $this->get('/minimal/demos/single-column');
        $response->assertStatus(200);
    }

    public function testFixHeader()
    {
        $response = $this->get('/minimal/demos/fix-header');
        $response->assertStatus(200);
    }

    public function testFixSidebar()
    {
        $response = $this->get('/minimal/demos/fix-sidebar');
        $response->assertStatus(200);
    }

    public function testFixHeaderSidebar()
    {
        $response = $this->get('/minimal/demos/fix-header-sidebar');
        $response->assertStatus(200);
    }
}
