<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;

class HorizontalTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testService()
    {
        $sidebar = App::make('App\Services\SidebarMenuService');
        $pages = $sidebar->getMenuItems();

        foreach ( $pages as $group ) {
            if(is_object($group)) {
                if(property_exists($group, 'children')) {
                   $this->accessPage($group->children);
                }
            }
        }
    }

    public function accessPage($groups){
        foreach ( $groups as $group ) {
            if(property_exists($group, 'url')){

                $response = $this->get($group->url);
                $response->assertStatus(200);

                if(property_exists($group, 'children')) {
                    $this->accessPage($group->children);
                }
            }
        }
    }

    public function testRedirect()
    {
        $response = $this->get('/horizontal');
        $response->assertRedirect('/horizontal/php-to-js-vars');
    }

    public function testHomeOne()
    {
        $response = $this->get('/horizontal/home-one');
        $response->assertStatus(200);
        $response->assertSeeText('Home One');
    }

    public function testHomeTwo()
    {
        $response = $this->get('/horizontal/home-two');
        $response->assertStatus(200);
        $response->assertSeeText('Home Two');
    }

    public function testPHPToJSVars()
    {
        $response = $this->get('/horizontal/php-to-js-vars');
        $response->assertStatus(200);
    }

    public function testBladeComponents()
    {
        $response = $this->get('/horizontal/blade-components');
        $response->assertStatus(200);
    }

    public function testBladeLoops()
    {
        $response = $this->get('/horizontal/blade-loops');
        $response->assertStatus(200);
    }

    public function testBoxed()
    {
        $response = $this->get('/horizontal/demos/boxed');
        $response->assertStatus(200);
    }

    public function testLogoCenter()
    {
        $response = $this->get('/horizontal/demos/logo-center');
        $response->assertStatus(200);
    }

    public function testSingleColumn()
    {
        $response = $this->get('/horizontal/demos/single-column');
        $response->assertStatus(200);
    }

    public function testFixHeader()
    {
        $response = $this->get('/horizontal/demos/fix-header');
        $response->assertStatus(200);
    }

    public function testFixSidebar()
    {
        $response = $this->get('/horizontal/demos/fix-sidebar');
        $response->assertStatus(200);
    }

    public function testFixHeaderSidebar()
    {
        $response = $this->get('/horizontal/demos/fix-header-sidebar');
        $response->assertStatus(200);
    }
}
