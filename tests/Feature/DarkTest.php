<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;

class DarkTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testService()
    {
        $sidebar = App::make('App\Services\SidebarMenuService');
        $pages = $sidebar->getMenuItems();

        foreach ( $pages as $group ) {
            if(is_object($group)) {
                if(property_exists($group, 'children')) {
                   $this->accessPage($group->children);
                }
            }
        }
    }

    public function accessPage($groups){
        foreach ( $groups as $group ) {
            if(property_exists($group, 'url')){

                $response = $this->get($group->url);
                $response->assertStatus(200);

                if(property_exists($group, 'children')) {
                    $this->accessPage($group->children);
                }
            }
        }
    }

    public function testRedirect()
    {
        $response = $this->get('/dark');
        $response->assertRedirect('/dark/php-to-js-vars');
    }

    public function testHomeOne()
    {
        $response = $this->get('/dark/home-one');
        $response->assertStatus(200);
        $response->assertSeeText('Home One');
    }

    public function testHomeTwo()
    {
        $response = $this->get('/dark/home-two');
        $response->assertStatus(200);
        $response->assertSeeText('Home Two');
    }

    public function testPHPToJSVars()
    {
        $response = $this->get('/dark/php-to-js-vars');
        $response->assertStatus(200);
    }

    public function testBladeComponents()
    {
        $response = $this->get('/dark/blade-components');
        $response->assertStatus(200);
    }

    public function testBladeLoops()
    {
        $response = $this->get('/dark/blade-loops');
        $response->assertStatus(200);
    }

    public function testBoxed()
    {
        $response = $this->get('/dark/demos/boxed');
        $response->assertStatus(200);
    }

    public function testLogoCenter()
    {
        $response = $this->get('/dark/demos/logo-center');
        $response->assertStatus(200);
    }

    public function testSingleColumn()
    {
        $response = $this->get('/dark/demos/single-column');
        $response->assertStatus(200);
    }

    public function testFixHeader()
    {
        $response = $this->get('/dark/demos/fix-header');
        $response->assertStatus(200);
    }

    public function testFixSidebar()
    {
        $response = $this->get('/dark/demos/fix-sidebar');
        $response->assertStatus(200);
    }

    public function testFixHeaderSidebar()
    {
        $response = $this->get('/dark/demos/fix-header-sidebar');
        $response->assertStatus(200);
    }
}
