<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;

class MonsterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testService()
    {
        $sidebar = App::make('App\Services\SidebarMenuService');
        $pages = $sidebar->getMenuItems();

        foreach ( $pages as $group ) {
            if(is_object($group)) {
                if(property_exists($group, 'children')) {
                   $this->accessPage($group->children);
                }
            }
        }
    }

    public function accessPage($groups){
        foreach ( $groups as $group ) {
            if(property_exists($group, 'url')){

                $response = $this->get($group->url);
                $response->assertStatus(200);

                if(property_exists($group, 'children')) {
                    $this->accessPage($group->children);
                }
            }
        }
    }

    public function testRedirect()
    {
        $response = $this->get('/monster');
        $response->assertRedirect('/monster/php-to-js-vars');
    }

    public function testHomeOne()
    {
        $response = $this->get('/monster/home-one');
        $response->assertStatus(200);
        $response->assertSeeText('Home One');
    }

    public function testHomeTwo()
    {
        $response = $this->get('/monster/home-two');
        $response->assertStatus(200);
        $response->assertSeeText('Home Two');
    }

    public function testPHPToJSVars()
    {
        $response = $this->get('/monster/php-to-js-vars');
        $response->assertStatus(200);
    }

    public function testBladeComponents()
    {
        $response = $this->get('/monster/blade-components');
        $response->assertStatus(200);
    }

    public function testBladeLoops()
    {
        $response = $this->get('/monster/blade-loops');
        $response->assertStatus(200);
    }

    public function testBoxed()
    {
        $response = $this->get('/monster/demos/boxed');
        $response->assertStatus(200);
    }

    public function testLogoCenter()
    {
        $response = $this->get('/monster/demos/logo-center');
        $response->assertStatus(200);
    }

    public function testSingleColumn()
    {
        $response = $this->get('/monster/demos/single-column');
        $response->assertStatus(200);
    }

    public function testFixHeader()
    {
        $response = $this->get('/monster/demos/fix-header');
        $response->assertStatus(200);
    }

    public function testFixSidebar()
    {
        $response = $this->get('/monster/demos/fix-sidebar');
        $response->assertStatus(200);
    }

    public function testFixHeaderSidebar()
    {
        $response = $this->get('/monster/demos/fix-header-sidebar');
        $response->assertStatus(200);
    }
}
