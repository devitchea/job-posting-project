<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUserDepToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_firstname')->nullable();
            $table->string('user_lastname')->nullable();
            $table->string('user_gender')->nullable();
            $table->string('user_phone')->nullable();
            $table->string('user_status')->nullable();

            $table->unsignedInteger('user_dep')->nullable();
            $table->foreign('user_dep')->references('id')->on('tbl_departments')->onDelete('cascade');

            $table->unsignedInteger('user_role_id')->nullable();
            $table->foreign('user_role_id')->references('id')->on('roles')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
