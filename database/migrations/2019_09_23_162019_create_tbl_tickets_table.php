<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tic_summary')->nullable();
            $table->string('tic_detail')->nullable();
            $table->text('tic_image')->nullable();
            $table->integer('tic_status')->nullable();
            $table->string('tic_type')->nullable();
            $table->string('tic_due_date')->nullable();
            $table->integer('tic_priority')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('assign_id')->nullable();
            $table->foreign('assign_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tickets');
    }
}
