<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["id" => 1 , "name" => "Super Admin", "remark" => "Full Access"],
            ["id" => 2 , "name" => "Admin", "remark" => "Access"],
            ["id" => 3 , "name" => "Customer", "remark" => "Customer"],
        ];
        foreach ($data as $record) {
            \App\Models\Role::create($record);
        }
    }
}
