<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Authentication Routes
 */

use Illuminate\Support\Facades\Route;


Auth::routes();


Route::get('/', function () {
    return view('/');
});

/*
 * When visiting root, redirect to first home   
 */
Route::view('/', 'auth.login');
Route::redirect('/home', '/', 302);

/*
 * When visiting root, redirect to first home   
 */
Route::get('auth2redirect', 'Auth\LoginController@redirectToProvider');
Route::get('auth2callback', 'Auth\LoginController@handleProviderCallback');

/*
 * Demos for page preset
 */

Route::get('/', 'MainBoardReportController@index');
Route::redirect('/admin', '/monster/dashboard', 302);

Route::prefix('admin')->group(function () {

    /*
 * Main Navigation
 */
    Route::get('/', 'UserPermissionController@index');

    /*
|--------------------------------------------------------------------------
|users manager Modules
|--------------------------------------------------------------------------
|
|   Route to all menu in Vehicles manager
|
*/
    /*
 * users
 */

    Route::get('user', 'UserController@index');
    Route::get('user/create', 'UserController@create');
    Route::post('user/store/{id?}', 'UserController@store');
    Route::get('user/destroy/{id?}', 'UserController@destroy');
    Route::get('user/show/{id?}', 'UserController@show');
    /*
* roles    
*/

    Route::get('role', 'UserRoleController@index');
    Route::get('role/create', 'UserRoleController@create');
    Route::post('role/store/{id?}', 'UserRoleController@store');
    Route::get('role/destroy/{id?}', 'UserRoleController@destroy');
    Route::get('role/show/{id?}', 'UserRoleController@show');

    // department
    Route::get('lookUp', 'DepartmentController@lookUp');
    // department
    Route::get('department', 'DepartmentController@index');
    Route::get('department/create', 'DepartmentController@create');
    Route::post('department/store/{id?}', 'DepartmentController@store');
    Route::get('department/destroy/{id}', 'DepartmentController@destroy');
    Route::get('department/show/{id?}', 'DepartmentController@show');


    // ticket lists

    Route::get('ticket', 'TicketController@index');
    Route::get('ticket/create', 'TicketController@create');
    Route::post('ticket/store/{id?}', 'TicketController@store');
    Route::get('ticket/destroy/{id}', 'TicketController@destroy');
    Route::get('ticket/show/{id?}', 'TicketController@show');
    Route::get('ticket/detail/{id?}', 'TicketController@detail');
    Route::post('ticket/storecomment/{id?}', 'TicketController@storecomment');
    Route::get('ticket/destroyMessage/{id}', 'TicketController@destroyMessage');
    Route::get('ticket/editcoment/{id}', 'TicketController@editcoment');

    Route::post('ticket/storecom', 'TicketController@storecom')->name('admin.ticket.storecom');
    
    Route::get('ticketclose', 'TicketCloseController@index');
    Route::get('ticketclose/create', 'TicketCloseController@create');
    Route::post('ticketclose/store/{id?}', 'TicketCloseController@store');
    Route::get('ticketclose/destroy/{id}', 'TicketCloseController@destroy');
    Route::get('ticketclose/show/{id?}', 'TicketCloseController@show');

    Route::get('ticketresolved', 'TicketResolvedController@index');
    Route::get('ticketresolved/create', 'TicketResolvedController@create');
    Route::post('ticketresolved/store/{id?}', 'TicketResolvedController@store');
    Route::get('ticketresolved/destroy/{id}', 'TicketResolvedController@destroy');
    Route::get('ticketresolved/show/{id?}', 'TicketResolvedController@show');


    Route::get('ticketreoverdue', 'TicketOverdueController@index');
    Route::get('ticketreoverdue/create', 'TicketOverdueController@create');
    Route::post('ticketreoverdue/store/{id?}', 'TicketOverdueController@store');
    Route::get('ticketreoverdue/destroy/{id}', 'TicketOverdueController@destroy');
    Route::get('ticketreoverdue/show/{id?}', 'TicketOverdueController@show');
    //send email

    Route::get('/sendemail', 'SendEmailController@index');
    Route::post('/sendemail/send', 'SendEmailController@send');
});
